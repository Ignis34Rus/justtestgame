// Fill out your copyright notice in the Description page of Project Settings.

#include "JTGCharacter.h"
#include "Interactable.h"
#include "GameplaySystemComponent.h"
#include "CraftingSystemComponent.h"
#include "InventorySystemComponent.h"
#include "InteractionSystemComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

AJTGCharacter::AJTGCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraAnchor = CreateDefaultSubobject<USpringArmComponent>("CameraAnchor");
	GameplaySystem = CreateDefaultSubobject<UGameplaySystemComponent>("GameplaySystem");
	CraftingSystem = CreateDefaultSubobject<UCraftingSystemComponent>("CraftingSystem");
	InventorySystem = CreateDefaultSubobject<UInventorySystemComponent>("MainInventory");
	InteractionSystem = CreateDefaultSubobject<UInteractionSystemComponent>("InteractionSystem");

	bUseControllerRotationYaw = false;
	PrimaryActorTick.bCanEverTick = true;


	CameraAnchor->RelativeLocation = FVector(0, 0, 15);
	CameraAnchor->bUsePawnControlRotation = true;

	auto* capsule = GetCapsuleComponent();
	auto* movement = GetCharacterMovement();
	
	movement->AirControl = 0.2;
	movement->JumpZVelocity = 600;
	movement->RotationRate = FRotator(0, 540, 0);
	movement->bOrientRotationToMovement = true;

	Camera->SetupAttachment(CameraAnchor);
	CameraAnchor->SetupAttachment(capsule);
	InteractionSystem->SetupAttachment(RootComponent);
	InteractionSystem->OnInteractableCandidateRemoved.AddDynamic(this, &AJTGCharacter::OnInteractableCandidateRemoved);
}

void AJTGCharacter::Tick(float DeltaTime)
{
	auto bestInteractionCandidate = InteractionSystem->GetBestInteractable(Camera);

	if (bestInteractionCandidate)
	{
		if (bestInteractionCandidate != BestInteractionCandidate)
		{
			OnBestInteractableCandidateChanged.Broadcast(BestInteractionCandidate, bestInteractionCandidate);

			BestInteractionCandidate = bestInteractionCandidate;
		}
	}
	else if(BestInteractionCandidate != nullptr)
	{
		OnBestInteractableCandidateChanged.Broadcast(BestInteractionCandidate, nullptr);
		BestInteractionCandidate = nullptr;
	}

	Super::Tick(DeltaTime);
}

void AJTGCharacter::OnInteractableCandidateRemoved(const TScriptInterface<IInteractable> candidate)
{
	if (BestInteractionCandidate == candidate.GetObject())
	{
		OnBestInteractableCandidateChanged.Broadcast(BestInteractionCandidate, nullptr);
		BestInteractionCandidate = nullptr;
	}
}
