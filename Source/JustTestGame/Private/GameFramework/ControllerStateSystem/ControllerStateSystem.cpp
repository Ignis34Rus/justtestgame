// Fill out your copyright notice in the Description page of Project Settings.

#include "ControllerStateSystem.h"
#include "ControllerState.h"
#include "ControllerStateSystemStorage.h"

UControllerState* UControllerStateSystem::GetState(const FGameplayTag& id)
{
	auto& stateInfo = Storage->GetControllerStateInfo(id);

	return NewObject<UControllerState>((UObject*)GetTransientPackage(), stateInfo.StateClass);
}
