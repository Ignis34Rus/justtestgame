// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractionSystemComponent.h"
#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Components/PrimitiveComponent.h"

UInteractionSystemComponent::UInteractionSystemComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;

	Super::InitSphereRadius(256);
	Super::SetGenerateOverlapEvents(true);
	Super::SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Super::SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	Super::OnComponentBeginOverlap.AddDynamic(this, &UInteractionSystemComponent::InteractionOnOverlapBegin);
	Super::OnComponentEndOverlap.AddDynamic(this, &UInteractionSystemComponent::InteractionOnOverlapEnd);
}

TScriptInterface<IInteractable> UInteractionSystemComponent::GetBestInteractable(const UCameraComponent* camera) const
{
	float max = -1;
	TScriptInterface<IInteractable> result;

	for (auto& candidate : Candidates) 
	{
		auto obj = candidate.GetObject();

		if (obj && obj->IsValidLowLevel())
		{
			auto diff = (IInteractable::Execute_GetPosition(obj) - camera->GetComponentLocation());

			diff.Normalize();

			auto dot = FVector::DotProduct(diff, camera->GetForwardVector());

			if (dot >= MinInteractionDot && max < dot)
			{
				max = dot;
				result = candidate;
			}
		}
	}

	return result;
}

void UInteractionSystemComponent::InteractionOnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
	{
		Candidates.Add(OtherActor);
	}
}

void UInteractionSystemComponent::InteractionOnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor->GetClass()->ImplementsInterface(UInteractable::StaticClass()))
	{
		Candidates.Remove(OtherActor);

		OnInteractableCandidateRemoved.Broadcast(OtherActor);
	}
}
