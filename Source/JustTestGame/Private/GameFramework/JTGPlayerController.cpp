// Fill out your copyright notice in the Description page of Project Settings.

#include "JTGPlayerController.h"
#include "JTGGameMode.h"
#include "JTGCharacter.h"
#include "Interactable.h"
#include "ItemBase.h"
#include "ItemPickUp.h"
#include "SavingSystem.h"
#include "JTGGameInstance.h"
#include "ControllerState.h"
#include "ControllerStateSystem.h"
#include "InventorySystem.h"
#include "NavigationSystem.h"
#include "Camera/CameraComponent.h"
#include "InteractionSystemComponent.h"
#include "InventorySystemComponent.h"
#include "CraftingSystemComponent.h"
#include "InventoryWidget.h"
#include "Kismet/KismetMathLibrary.h"


AJTGPlayerController::AJTGPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}


void AJTGPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	auto* character = GetCharacter();

	JTGCharacter = character ? Cast<AJTGCharacter>(character) : nullptr;

	if (true)
	{

	}
}

void AJTGPlayerController::SaveGame()
{
	auto playerInfo = FPlayerStateInfo();
	auto gameMode = GetWorld()->GetAuthGameMode<AJTGGameMode>();
	auto savingSystem = gameMode->GetSavingSystem();
	auto inventorySystem = gameMode->GetInventorySystem();

	for (auto& slot : JTGCharacter->GetInventorySystem()->GetSlots())
	{
		playerInfo.MainInventory.Add(FItemStackInfo(slot));
	}

	playerInfo.Recipes = JTGCharacter->GetCraftingSystem()->GetRecipes();
	playerInfo.Position = JTGCharacter->GetActorLocation();
	playerInfo.Rotation = JTGCharacter->GetActorRotation();
	playerInfo.ControlRotation = GetControlRotation();

	savingSystem->SavePlayer(playerInfo);
	inventorySystem->SaveState(savingSystem);
	savingSystem->SaveGame(GetGameInstance<UJTGGameInstance>()->CurrentSaveSlot);
}

void AJTGPlayerController::OnBestInteractableCandidateChanged_Implementation(const TScriptInterface<IInteractable>& oldCandidate, const TScriptInterface<IInteractable>& newCandidate)
{
	if (oldCandidate)
	{
		auto obj = oldCandidate.GetObject();

		if (obj)
		{
			IInteractable::Execute_SetHighlight(obj, this, false);
		}
	}

	if (newCandidate)
	{
		auto obj = newCandidate.GetObject();

		if (obj)
		{
			IInteractable::Execute_SetHighlight(obj, this, true);
		}
	}
}

void AJTGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// movement/rotation input
	InputComponent->BindAxis("Turn", this, &AJTGPlayerController::InputTurn);
	InputComponent->BindAxis("LookUp", this, &AJTGPlayerController::InputLookUp);
	InputComponent->BindAxis("MoveRight", this, &AJTGPlayerController::InputMoveRight);
	InputComponent->BindAxis("MoveForward", this, &AJTGPlayerController::InputMoveForward);

	// actions input
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &AJTGPlayerController::InputJumpPressed);
	InputComponent->BindAction("Jump", EInputEvent::IE_Released, this, &AJTGPlayerController::InputJumpReleased);
	InputComponent->BindAction("InventoryToggle", EInputEvent::IE_Pressed, this, &AJTGPlayerController::InputInventoryTogglePressed);
	InputComponent->BindAction("Interact", EInputEvent::IE_Pressed, this, &AJTGPlayerController::InputInteractPressed);
	InputComponent->BindAction("SaveGame", EInputEvent::IE_Pressed, this, &AJTGPlayerController::InputSaveGamePressed);
	InputComponent->BindAction("InGameMenu", EInputEvent::IE_Pressed, this, &AJTGPlayerController::InputInGameMenuPressed);
}

void AJTGPlayerController::BeginPlayingState()
{
	if (JTGCharacter)
	{
		JTGCharacter->OnBestInteractableCandidateChanged.AddDynamic(this, &AJTGPlayerController::OnBestInteractableCandidateChanged);
		JTGCharacter->GetInventorySystem()->OnSlotDropped.AddDynamic(this, &AJTGPlayerController::OnSlotDropped);

		if (FirstTimeLoad)
		{
			auto gameMode = GetWorld()->GetAuthGameMode<AJTGGameMode>();
			auto savingSystem = gameMode->GetSavingSystem();
			auto& playerState = savingSystem->GetPlayerState();

			if (gameMode->GameWasLoaded())
			{
				auto inventorySystem = gameMode->GetInventorySystem();

				TArray<FItemStack> items;

				for (auto item : playerState.MainInventory)
				{
					items.Add(FItemStack
						{
							inventorySystem->GetItem(item.Guid),
							item.Index,
							item.Amount,
						});
				}

				JTGCharacter->GetCraftingSystem()->LoadState(playerState.Recipes);
				JTGCharacter->GetInventorySystem()->LoadState(items);
				JTGCharacter->SetActorLocationAndRotation(playerState.Position, playerState.Rotation, false, nullptr, ETeleportType::ResetPhysics);
				RotationInput = playerState.ControlRotation;
			}

			FirstTimeLoad = false;
		}
	}
}

void AJTGPlayerController::InputTurn(float value)
{
	JTGCharacter->AddControllerYawInput(value);
}

void AJTGPlayerController::InputLookUp(float value)
{
	JTGCharacter->AddControllerPitchInput(value);
}

void AJTGPlayerController::InputMoveRight(float value)
{
	FRotator rotator = JTGCharacter->GetControlRotation();
	rotator.Roll = 0;
	rotator.Pitch = 0;
	JTGCharacter->AddMovementInput(UKismetMathLibrary::GetRightVector(rotator), value);
}

void AJTGPlayerController::InputMoveForward(float value)
{
	FRotator rotator = JTGCharacter->GetControlRotation();
	rotator.Roll = 0;
	rotator.Pitch = 0;
	JTGCharacter->AddMovementInput(UKismetMathLibrary::GetForwardVector(rotator), value);
}

void AJTGPlayerController::InputJumpPressed()
{
	JTGCharacter->Jump();
}

void AJTGPlayerController::InputJumpReleased()
{
	JTGCharacter->StopJumping();
}

void AJTGPlayerController::InputInventoryTogglePressed()
{
	if (!InventoryWidget)
	{
		if (!InventoryWidgetClass)
		{
			return;
		}

		InventoryWidget = NewObject<UInventoryWidget>(this, InventoryWidgetClass);
		InventoryWidget->InventorySystem = JTGCharacter->GetInventorySystem();
	}

	if (InventoryWidget->IsInViewport())
	{
		InventoryWidget->RemoveFromViewport();
		PopState();
	}
	else
	{
		InventoryWidget->AddToViewport();
		PushState(InventoryStateTag);
	}
}

void AJTGPlayerController::InputInteractPressed()
{
	auto candidate = JTGCharacter->GetBestInteractionCandidate();

	if (candidate != nullptr)
	{
		IInteractable::Execute_Interact(candidate.GetObject(), this);
	}
}

void AJTGPlayerController::InputSaveGamePressed()
{
	SaveGame();
}

void AJTGPlayerController::InputInGameMenuPressed() 
{
	if (!InGameMenuWidget)
	{
		if (!InGameMenuWidgetClass)
		{
			return;
		}

		InGameMenuWidget = NewObject<UUserWidget>(this, InGameMenuWidgetClass);
		InGameMenuWidget->SetOwningPlayer(this);
	}

	if (InGameMenuWidget->IsInViewport())
	{
		InGameMenuWidget->RemoveFromViewport();
		PopState();
	}
	else
	{
		InGameMenuWidget->AddToViewport();
		PushState(IngameMenuStateTag);
	}
}

void AJTGPlayerController::PopState()
{
	States.Pop()->LeaveState(this);

	if (States.Num())
	{
		States.Top()->EnterState(this);
	}
}

void AJTGPlayerController::PushState(const FGameplayTag& id)
{
	auto state = GetWorld()->GetAuthGameMode<AJTGGameMode>()->GetControllerStateSystem()->GetState(id);

	if (States.Num())
	{
		States.Top()->LeaveState(this);
	}

	state->EnterState(this);

	States.Push(state);
}

void AJTGPlayerController::OnSlotDropped(const FItemStack& slot)
{
	if (slot.Item->PickUpVisual)
	{
		auto navigationSystem = UNavigationSystemV1::GetCurrent(GetWorld());
		FNavLocation location;

		auto origin = JTGCharacter->GetActorLocation() + JTGCharacter->GetActorForwardVector() * 64;

		if (navigationSystem->GetRandomPointInNavigableRadius(JTGCharacter->GetActorLocation(), 128, location))
		{
			auto actor = GetWorld()->SpawnActor<AItemPickUp>(slot.Item->PickUpVisual, FTransform(location.Location));

			actor->ItemGuid = slot.Item->Guid;
			actor->Amount = slot.Amount;
		}
	}
}
