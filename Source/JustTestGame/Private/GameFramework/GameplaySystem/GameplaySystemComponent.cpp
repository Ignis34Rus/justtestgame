#include "GameplaySystemComponent.h"
#include "JTGGameMode.h"
#include "Engine/World.h"
#include "GameplaySystem.h"
#include "GameplaySystemStorage.h"
#include "GameplayAttributeFunction.h"

UGameplaySystemComponent::UGameplaySystemComponent(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UGameplaySystemComponent::BeginPlay()
{
	auto gameplaySystemStorage = GetWorld()->GetAuthGameMode<AJTGGameMode>()->GetGameplaySystem()->GetStorage();

	for (auto& attribute : Attributes) 
	{
		attribute.Value.Recalculate();

		auto functionClass = AttributeFunctionClasses.FindRef(attribute.Key);

		if (!functionClass)
		{
			functionClass = gameplaySystemStorage->GetAttributeInfo(attribute.Key).FunctionClass;
		}

		if (functionClass)
		{
			AttributeFunctions.Add(NewObject<UGameplayAttributeFunction>(GetTransientPackage(), functionClass));
		}
	}

	Super::BeginPlay();
}

void UGameplaySystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	for (auto function : AttributeFunctions) 
	{
		function->Tick(DeltaTime, this);
	}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UGameplaySystemComponent::SetAttributeCurrentValue(const FGameplayTag& id, const float value)
{
	auto& attribute = Attributes.FindChecked(id);

	const auto oldCurrent = attribute.GetCurrent();
	attribute.SetCurrent(value);
	const auto newCurrent = attribute.GetCurrent();
	if (oldCurrent != newCurrent)
	{
		OnAttributeCurrentValueChanged.Broadcast(id, newCurrent, oldCurrent);
	}
}

void UGameplaySystemComponent::SetAttributeOriginalValue(const FGameplayTag& id, const float value)
{
	auto& attribute = Attributes.FindChecked(id);	
	const auto oldMaximum = attribute.GetMaximum();
	const auto oldCurrent = attribute.GetCurrent();
	attribute.SetOriginal(value);
	RiseAttributeValueChanged(id, oldMaximum, oldCurrent);
}

FGuid UGameplaySystemComponent::AddAttributeModifier(const FGameplayTag& id, float value)
{
	auto& attribute = Attributes.FindChecked(id);
	auto guid = FGuid::NewGuid();
	auto modificator = FGameplayAttributeModificator();
	modificator.Type = EGameplayAttributeModificatorType::Modifier;
	modificator.Value = value;
	modificator.AttributeId = id;
	const auto oldMaximum = attribute.GetMaximum();
	const auto oldCurrent = attribute.GetCurrent();
	attribute.AddModifier(value);
	AttributeModificators.Add(guid, modificator);
	RiseAttributeValueChanged(id, oldMaximum, oldCurrent);
	return guid;
}

FGuid UGameplaySystemComponent::AddAttributeMultiplier(const FGameplayTag& id, float value)
{
	auto& attribute = Attributes.FindChecked(id);
	auto guid = FGuid::NewGuid();
	auto modificator = FGameplayAttributeModificator();
	modificator.Type = EGameplayAttributeModificatorType::Multiplier;
	modificator.Value = value;
	modificator.AttributeId = id;
	const auto oldMaximum = attribute.GetMaximum();
	const auto oldCurrent = attribute.GetCurrent();
	attribute.AddMultiplier(value);
	AttributeModificators.Add(guid, modificator);
	RiseAttributeValueChanged(id, oldMaximum, oldCurrent);
	return guid;
}

bool UGameplaySystemComponent::RemoveAttributeModificator(const FGuid& guid)
{
	FGameplayAttributeModificator modificator;

	if (AttributeModificators.RemoveAndCopyValue(guid, modificator))
	{
		auto attribute = Attributes.Find(modificator.AttributeId);

		if (attribute) 
		{
			const auto oldMaximum = attribute->GetMaximum();
			const auto oldCurrent = attribute->GetCurrent();
			switch (modificator.Type)
			{
				case EGameplayAttributeModificatorType::Multiplier:
					attribute->RemoveMultiplier(modificator.Value);
					break;
				case EGameplayAttributeModificatorType::Modifier:
					attribute->RemoveModifier(modificator.Value);
					break;
			}
			RiseAttributeValueChanged(modificator.AttributeId, oldMaximum, oldCurrent);
			return true;
		}
	}

	return false;
}
