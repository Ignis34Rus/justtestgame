// Fill out your copyright notice in the Description page of Project Settings.

#include "ItemPickUp.h"
#include "JTGGameMode.h"
#include "Saveable.h"
#include "SavingSystem.h"
#include "JTGCharacter.h"
#include "JTGPlayerController.h"
#include "SavingSystem.h"
#include "InventorySystem.h"
#include "Kismet/KismetMathLibrary.h"
#include "InventorySystemComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SphereComponent.h"

AItemPickUp::AItemPickUp(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

	PrimaryActorTick.bCanEverTick = true;

	InteractionSphere = CreateDefaultSubobject<USphereComponent>("InteractionSphere");
	InteractionSphere->InitSphereRadius(128);
	InteractionSphere->SetGenerateOverlapEvents(true);
	InteractionSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	InteractionSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	InteractionWidget = CreateDefaultSubobject<UWidgetComponent>("InteractionWidget");
	InteractionWidget->RelativeLocation = FVector(0, 0, 80);
	InteractionWidget->SetVisibility(false);
	InteractionWidget->SetDrawAtDesiredSize(true);
	InteractionWidget->SetWidgetSpace(EWidgetSpace::Screen);

	InteractionWidget->SetupAttachment(InteractionSphere);
	RootComponent = InteractionSphere;
}

void AItemPickUp::Tick(float DeltaTime) 
{
	if (Controller)
	{
		InteractionWidget->SetWorldRotation(
			UKismetMathLibrary::FindLookAtRotation(
				InteractionWidget->GetComponentLocation(),
				Controller->PlayerCameraManager->GetCameraLocation()));
	}
}

FVector AItemPickUp::GetPosition_Implementation() const
{
	return InteractionSphere->GetCenterOfMass();
}

void AItemPickUp::Interact_Implementation(const AJTGPlayerController* controller)
{
	controller->GetJTGCharacter()->GetInventorySystem()->Add(Item, Amount);
	Destroy();
}

void AItemPickUp::SetHighlight_Implementation(const AJTGPlayerController* controller, bool value)
{
	InteractionWidget->SetVisibility(value, true);

	Controller = value ? controller : nullptr;
}

void AItemPickUp::BeginPlay()
{
	auto mode = GetWorld()->GetAuthGameMode<AJTGGameMode>();

	if (mode)
	{
		Item = mode->GetInventorySystem()->GetItem(ItemGuid);

	}

	Super::BeginPlay();
}
