// Fill out your copyright notice in the Description page of Project Settings.

#include "InventorySystemComponent.h"
#include "ItemBase.h"

int UInventorySystemComponent::Count(const UItemBase* item) const
{
	return CountCache.FindRef(item->Guid);
}

bool UInventorySystemComponent::CanBeAdded(const UItemBase* item, const int32 count)
{
	if (count < 0 ||
		!(Categories.Num() == 0 || Categories.HasAny(item->Categories)) ||
		(HasMaxSlots() && !item->IsStackable() && Slots.Num() >= MaxSlots))
	{
		return false;
	}

	if (HasMaxSlots())
	{
		if (item->IsStackable())
		{
			auto space = (MaxSlots - Slots.Num()) * item->StackSize;

			if (space > count)
			{
				return true;
			}

			space = count - space;

			const auto MaxStack = item->StackSize;

			for (int32 i = Slots.Num() - 1; i >= 0; i--)
			{
				auto& slot = Slots[i];

				if (slot.Item != nullptr && slot.Item->Equals(item) && slot.Amount < MaxStack)
				{
					space -= (MaxStack - slot.Amount);

					if (space <= 0)
					{
						return true;
					}
				}
			}

			return space <= 0;
		}
		else
		{
			return (MaxSlots - Slots.Num()) >= count;
		}
	}
	else
	{
		return true;
	}
}

int UInventorySystemComponent::Add(const UItemBase* item, const int32 count)
{
	if (count <= 0 || !(Categories.Num() == 0 || Categories.HasAny(item->Categories)))
	{
		return count;
	}

	auto& iCount = CountCache.FindOrAdd(item->Guid);

	auto remaining = count;

	const auto MaxStack = item->StackSize;

	if (item->IsStackable())
	{
		for (int32 i = Slots.Num() - 1; i >= 0; i--)
		{
			auto& slot = Slots[i];

			if (slot.Item != nullptr && slot.Item->Equals(item) && slot.Amount < MaxStack)
			{
				auto space = MaxStack - slot.Amount;

				if (space >= remaining)
				{
					slot.Amount += remaining;
					iCount += remaining;
					remaining = 0;
				}
				else
				{
					slot.Amount += space;
					iCount += space;
					remaining -= space;
				}

				OnSlotChanged.Broadcast(slot);

				if (remaining == 0)
				{
					break;
				}
			}
		}
	}

	for (int index = GetFreeIndex(); remaining > 0 && index >= 0; index = GetFreeIndex())
	{
		auto& slot = Slots.InsertDefaulted_GetRef(index);

		slot.Item = item;
		slot.Index = index;

		if (MaxStack >= remaining)
		{
			slot.Amount = remaining;
			iCount += remaining;
			remaining = 0;
		}
		else
		{
			slot.Amount = MaxStack;
			iCount += MaxStack;
			remaining -= MaxStack;
		}

		OnSlotChanged.Broadcast(slot);
	}

	return remaining;
}

int UInventorySystemComponent::Remove(const UItemBase* item, const int32 count)
{
	if (item == nullptr || count <= 0 || !(Categories.Num() == 0 || Categories.HasAny(item->Categories)))
	{
		return count;
	}

	if (CountCache.Contains(item->Guid))
	{
		auto& iCount = CountCache.FindOrAdd(item->Guid);

		if (iCount > count)
		{
			iCount -= count;
		}
		else
		{
			CountCache.Remove(item->Guid);
		}

		auto remaining = count;

		for (int32 i = Slots.Num() - 1; i >= 0; i--)
		{
			auto& slot = Slots[i];

			if (slot.Item != nullptr && slot.Item->Equals(item))
			{
				if (slot.Amount <= remaining)
				{
					remaining -= slot.Amount;
					slot.Amount = 0;
					slot.Item = nullptr;
					auto oldSlot = slot;
					Slots.RemoveAt(i);
					OnSlotChanged.Broadcast(oldSlot);
				}
				else
				{
					slot.Amount -= remaining;
					remaining = 0;
					OnSlotChanged.Broadcast(slot);
				}

				if (remaining == 0)
				{
					break;
				}
			}
		}

		return remaining;
	}

	return count;
}

bool UInventorySystemComponent::RemoveSlot(const int32 slotIndex, FItemStack& stack)
{
	int32 slotArrayIndex;

	auto slot = GetSlot(slotIndex, slotArrayIndex);

	if (slot.Item == nullptr)
	{
		Slots.RemoveAt(slotArrayIndex);
		return false;
	}
	else
	{
		CountCache[slot.Item->Guid] -= slot.Amount;
		Slots.RemoveAt(slotArrayIndex);
		stack = slot;
		slot.Item = nullptr;
		slot.Amount = 0;
		OnSlotChanged.Broadcast(slot);
		return true;
	}
}

bool UInventorySystemComponent::Move(const int32 sourceSlotIndex, const int32 destinationSlotIndex)
{
	if (HasMaxSlots() && (sourceSlotIndex >= MaxSlots || destinationSlotIndex >= MaxSlots))
	{
		return false;
	}

	int32 sourceArrayIndex, destinationArrayIndex;
	
	auto& destinationSlot = GetSlot(destinationSlotIndex, destinationArrayIndex);
	auto& sourceSlot = GetSlot(sourceSlotIndex, sourceArrayIndex);

	if (sourceSlot.Item != nullptr &&
		destinationSlot.Item != nullptr &&
		sourceSlot.Item->Equals(destinationSlot.Item) &&
		sourceSlot.Item->IsStackable() &&
		destinationSlot.Amount < sourceSlot.Item->StackSize)
	{
		auto space = sourceSlot.Item->StackSize - destinationSlot.Amount;

		if (space >= sourceSlot.Amount)
		{
			destinationSlot.Amount += sourceSlot.Amount;
			sourceSlot.Item = nullptr;
			sourceSlot.Amount = 0;
		}
		else
		{
			sourceSlot.Amount -= space;
			destinationSlot.Amount += space;
		}
	}
	else
	{
		auto sourceSlotCopy = sourceSlot;

		sourceSlot.Item = destinationSlot.Item;
		sourceSlot.Amount = destinationSlot.Amount;

		destinationSlot.Item = sourceSlotCopy.Item;
		destinationSlot.Amount = sourceSlotCopy.Amount;
	}

	OnSlotChanged.Broadcast(sourceSlot);
	OnSlotChanged.Broadcast(destinationSlot);

	if (sourceArrayIndex > destinationArrayIndex)
	{
		if (sourceSlot.Item == nullptr)
		{
			Slots.RemoveAt(sourceArrayIndex);
		}

		if (destinationSlot.Item == nullptr)
		{
			Slots.RemoveAt(destinationArrayIndex);
		}
	}
	else
	{
		if (destinationSlot.Item == nullptr)
		{
			Slots.RemoveAt(destinationArrayIndex);
		}

		if (sourceSlot.Item == nullptr)
		{
			Slots.RemoveAt(sourceArrayIndex);
		}
	}

	return true;
}

void UInventorySystemComponent::Compact()
{
	for (int32 i = 0; i < Slots.Num(); i++)
	{
		auto& slot = Slots[i];

		if (slot.Index != i)
		{
			slot.Index = i;

			OnSlotChanged.Broadcast(slot);
		}
	}
}

TArray<FItemStack> UInventorySystemComponent::GetSlots()
{
	auto result = TArray<FItemStack>();
	result.Append(Slots);
	return result;
}

bool UInventorySystemComponent::RequestDrop(int slotIndex)
{
	FItemStack stack;

	if (RemoveSlot(slotIndex, stack))
	{
		OnSlotDropped.Broadcast(stack);
		return true;
	}

	return false;
}

void UInventorySystemComponent::LoadState(const TArray<FItemStack>& state)
{
	bool useCompact = false;
	TSet<int> usedIndexes;

	Slots.Empty();
	CountCache.Empty();

	for (auto& slot : state) 
	{
		if (usedIndexes.Contains(slot.Index))
		{
			useCompact = true;
		}

		usedIndexes.Add(slot.Index);

		Slots.Add(slot);
		CountCache.FindOrAdd(slot.Item->Guid) += slot.Amount;
	}

	if (useCompact)
	{
		Compact();
	}

	for (auto& slot : Slots) 
	{
		OnSlotChanged.Broadcast(slot);
	}
}

int UInventorySystemComponent::GetFreeIndex()
{
	auto result = 0;

	for (auto& slot : Slots) 
	{
		if (slot.Index == result)
		{
			result++;
		}
		else if (slot.Index > result)
		{
			break;
		}
	}

	return !HasMaxSlots() || result < MaxSlots ? result : -1;
}

FItemStack& UInventorySystemComponent::GetSlot(const int32 slotIndex, int32& arrayIndex)
{
	auto num = Slots.Num();

	if (slotIndex >= num)
	{
		for (int i = num - 1; i >= 0; i--)
		{
			if (slotIndex > Slots[i].Index)
			{
				arrayIndex = i + 1;

				auto& slot = Slots.InsertZeroed_GetRef(arrayIndex);

				slot.Index = slotIndex;

				return slot;
			}
			else if (slotIndex == Slots[i].Index)
			{
				arrayIndex = i;

				return Slots[i];
			}
		}

		arrayIndex = 0;

		auto& slot = Slots.InsertZeroed_GetRef(0);

		slot.Index = slotIndex;

		return slot;
	}
	else
	{
		for (int i = 0; i < num; i++)
		{
			if (slotIndex == Slots[i].Index)
			{
				arrayIndex = i;

				return Slots[i];
			}
			else if (slotIndex >= Slots[i].Index)
			{
				continue;
			}

			arrayIndex = i;

			auto& slot = Slots.InsertZeroed_GetRef(i);

			slot.Index = slotIndex;

			return slot;
		}

		arrayIndex = Slots.Num();

		auto& slot = Slots.AddZeroed_GetRef();

		slot.Index = slotIndex;

		return slot;
	}
}
