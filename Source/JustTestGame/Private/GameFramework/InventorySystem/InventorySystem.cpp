// Fill out your copyright notice in the Description page of Project Settings.

#include "InventorySystem.h"
#include "ItemBase.h"
#include "SavingSystem.h"
#include "InventorySystemStorage.h"
#include "GameplayTagsManager.h"

UItemBase* UInventorySystem::CreateItem(const FGameplayTag& id)
{
	return ConstructItem(FItemGuid{ id });
}

UItemBase* UInventorySystem::GetItem(const FItemGuid& guid) 
{
	if (guid.IsInstance())
	{
		return DynamicItemsCache.FindRef(guid);
	}

	// CreateItem ensures that UItemBase created only once for static items
	return CreateItem(guid.Id);
}

void UInventorySystem::LoadState(USavingSystem* savingSystem)
{
	for (auto& instance : savingSystem->GetItemsInstances())
	{
		auto item = ConstructItem(instance.Guid);

		if (item)
		{
			item->Sockets = instance.Sockets;
			item->StackSize = instance.StackSize;
			item->BasePrice = instance.BasePrice;
			item->ItemAttributes = instance.ItemAttributes;
			item->GameplayAttributes = instance.GameplayAttributes;
		}
	}
}

void UInventorySystem::SaveState(USavingSystem* savingSystem)
{
	for (auto& item : DynamicItemsCache)
	{
		savingSystem->SaveItemInstance(FItemInstanceInfo
			{ 
				item.Key,
				item.Value->StackSize,
				item.Value->BasePrice,
				item.Value->Sockets,
				item.Value->ItemAttributes,
				item.Value->GameplayAttributes
			});
	}
}

UItemBase* UInventorySystem::ConstructItem(const FItemGuid& guid)
{
	UItemBase* result = nullptr;

	if (guid.Id.IsValid())
	{
		if (guid.Uid == 0)
		{
			result = StaticItemsCache.FindRef(guid.Id);

			if (result)
			{
				return result;
			}
		}

		auto& itemInfo = Storage->GetItemInfo(guid.Id);

		result = NewObject<UItemBase>(this);
		result->InventorySystem = this;

		result->Guid = guid;
		result->Name = itemInfo.Name;
		result->Icon = itemInfo.Icon;
		result->Spell = itemInfo.Spell;
		result->Quality = itemInfo.Quality;
		result->Material = itemInfo.Material;
		result->Description = itemInfo.Description;
		result->PickUpVisual = itemInfo.PickUpVisual;

		result->Categories.AddTag(itemInfo.Category);
		result->Categories.AppendTags(itemInfo.SubCategories);

		result->GameplayAttributes.Append(itemInfo.GameplayAttributes);

		for (auto& socket : itemInfo.Sockets)
		{
			result->Sockets.Add(FItemSocketSlot{ socket });
		}

		{
			TArray<FGameplayTag> categories;

			itemInfo.Category
				.GetGameplayTagParents()
				.GetGameplayTagArray(categories);

			for (int i = categories.Num() - 1; i >= 0; i--)
			{
				auto& categoryInfo = Storage->GetCategoryInfo(categories[i]);

				result->ItemAttributes.Append(categoryInfo.ItemAttributes);
			}
		}

		{
			int32 value;

			if (result->ItemAttributes.RemoveAndCopyValue(BasePriceTag, value))
			{
				result->BasePrice = value;
			}

			if (result->ItemAttributes.RemoveAndCopyValue(StackSizeTag, value))
			{
				result->StackSize = value;
			}
			else
			{
				result->StackSize = 1;
			}
		}

		if (result->Guid.IsInstance())
		{
			DynamicItemsCache.Add(result->Guid, result);
		}
		else
		{
			StaticItemsCache.Add(result->Guid.Id, result);
		}
	}

	return result;
}
