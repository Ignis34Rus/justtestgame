// Fill out your copyright notice in the Description page of Project Settings.

#include "CraftingSystemComponent.h"

void UCraftingSystemComponent::Add(const FGameplayTag& recipe)
{
	bool alreadyInSet;
	Recipes.Add(recipe, &alreadyInSet);

	if (!alreadyInSet)
	{
		OnCraftingRecipesChanged.Broadcast();
	}
}

void UCraftingSystemComponent::Remove(const FGameplayTag& recipe)
{
	if (Recipes.Remove(recipe) > 0) 
	{
		OnCraftingRecipesChanged.Broadcast();
	}
}
