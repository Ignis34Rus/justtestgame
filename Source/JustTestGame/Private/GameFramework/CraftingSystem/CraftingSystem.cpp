// Fill out your copyright notice in the Description page of Project Settings.

#include "CraftingSystem.h"
#include "JTGGameMode.h"
#include "Engine/World.h"
#include "CraftingSystemStorage.h"
#include "InventorySystem.h"
#include "InventorySystemStorage.h"
#include "InventorySystemComponent.h"

bool UCraftingSystem::Craft(const FGameplayTag& recipe, UInventorySystemComponent* inventory)
{
	auto craftingInfo = GetCraftingInfo(recipe, inventory);

	if (craftingInfo.Requirements.Num() > 0)
	{
		for (auto& requirement : craftingInfo.Requirements)
		{
			if (!requirement.Item || (requirement.AmountRequired > requirement.AmountAvailable))
			{
				return false;
			}
		}

		auto inventorySystem = GetWorld()->GetAuthGameMode<AJTGGameMode>()->GetInventorySystem();
		
		auto item = inventorySystem->CreateItem(craftingInfo.Produce);
		
		if (item)
		{
			auto count = craftingInfo.ProduceMax;

			if (craftingInfo.ProduceMax != craftingInfo.ProduceMin)
			{
				count = FMath::RandRange(craftingInfo.ProduceMin, craftingInfo.ProduceMax);
			}

			if (inventory->CanBeAdded(item, count))
			{
				for (auto& requirement : craftingInfo.Requirements)
				{
					inventory->Remove(requirement.Item, requirement.AmountRequired);
				}

				return inventory->Add(item, count) == 0;
			}
		}
	}

	return false;
}

FCraftingInfo UCraftingSystem::GetCraftingInfo(const FGameplayTag& recipe, const UInventorySystemComponent* inventory) const
{
	auto result = FCraftingInfo();
	auto& recipeInfo = Storage->GetCraftingRecipeInfo(recipe);

	auto inventorySystem = GetWorld()->GetAuthGameMode<AJTGGameMode>()->GetInventorySystem();

	auto& produceInfo = inventorySystem->GetStorage()->GetItemInfo(recipeInfo.Produce);

	result.Id = recipe;
	result.Name = recipeInfo.Name;
	result.Produce = recipeInfo.Produce;
	result.ProduceName = produceInfo.Name;
	result.ProduceMin = recipeInfo.ProduceMin;
	result.ProduceMax = recipeInfo.ProduceMax;
	result.Description = recipeInfo.Description;

	for (auto& requirement : recipeInfo.Requirements)
	{
		auto item = inventorySystem->GetItem(FItemGuid{ requirement.ItemId });

		auto craftingRequirement = FCraftingRequirement();

		if (item)
		{
			craftingRequirement.Item = item;
			craftingRequirement.AmountAvailable = inventory->Count(item);
			craftingRequirement.AmountRequired = requirement.Amount;
		}
		else
		{
			craftingRequirement.Item = nullptr;
			craftingRequirement.AmountAvailable = 0;
			craftingRequirement.AmountRequired = requirement.Amount;
		}

		result.Requirements.Add(craftingRequirement);
	}

	return result;
}
