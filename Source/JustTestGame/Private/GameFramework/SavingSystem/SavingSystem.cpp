#include "SavingSystem.h"
#include "EngineUtils.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Saveable.h"
#include "JTGSaveGame.h"
#include "Kismet/GameplayStatics.h"

bool USavingSystem::LoadGame(const FSaveSlotInfo& slotInfo)
{
	if (!slotInfo.SlotName.IsEmpty() && slotInfo.SlotIndex >= 0 && UGameplayStatics::DoesSaveGameExist(slotInfo.SlotName, slotInfo.SlotIndex))
	{
		auto saveSlot = Cast<UJTGSaveGame>(
			UGameplayStatics::LoadGameFromSlot(slotInfo.SlotName, slotInfo.SlotIndex));

		if (saveSlot)
		{
			auto world = GetWorld();
			PlayerState = saveSlot->PlayerState;
			ItemsInstances = saveSlot->ItemsInstances;
			auto& states = saveSlot->WorldObjectStates;

			for (TActorIterator<AActor> Itr(world); Itr; ++Itr)
			{
				if (Itr->GetClass()->ImplementsInterface(USaveable::StaticClass()))
				{
					FWorldObjectState state;
					auto name = Itr->GetName();

					if (states.RemoveAndCopyValue(name, state))
					{
						ISaveable::Execute_LoadState(*Itr, state);
					}
					else
					{
						Itr->Destroy();
					}
				}
			}

			for (auto& pair : states)
			{
				auto actor = world->SpawnActor<AActor>(pair.Value.ObjectClass, pair.Value.Position, pair.Value.Rotation);

				if (actor->GetClass()->ImplementsInterface(USaveable::StaticClass())) 
				{
					ISaveable::Execute_LoadState(actor, pair.Value);
				}
			}

			return true;
		}
	}

	return false;
}

bool USavingSystem::SaveGame(const FSaveSlotInfo& slotInfo)
{
	if (!slotInfo.SlotName.IsEmpty() && slotInfo.SlotIndex >= 0)
	{
		auto saveSlot = Cast<UJTGSaveGame>(
			UGameplayStatics::CreateSaveGameObject(UJTGSaveGame::StaticClass()));

		TMap<FString, FWorldObjectState> states;

		for (TActorIterator<AActor> Itr(GetWorld()); Itr; ++Itr)
		{
			if (Itr->GetClass()->ImplementsInterface(USaveable::StaticClass()))
			{
				states.Add(Itr->GetName(), ISaveable::Execute_SaveState(*Itr));
			}
		}

		saveSlot->PlayerState = PlayerState;
		saveSlot->ItemsInstances = ItemsInstances;
		saveSlot->WorldObjectStates = states;

		return UGameplayStatics::SaveGameToSlot(saveSlot, slotInfo.SlotName, slotInfo.SlotIndex);
	}

	return false;
}

void USavingSystem::SaveItemInstance(const FItemInstanceInfo& instanceInfo)
{
	ItemsInstances.Add(instanceInfo);
}
