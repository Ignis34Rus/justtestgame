// Fill out your copyright notice in the Description page of Project Settings.


#include "JTGGameMode.h"
#include "JTGCharacter.h"
#include "JTGGameInstance.h"
#include "JTGPlayerController.h"
#include "SavingSystem.h"
#include "CraftingSystem.h"
#include "GameplaySystem.h"
#include "InventorySystem.h"
#include "ControllerStateSystem.h"

AJTGGameMode::AJTGGameMode()
{
	DefaultPawnClass = AJTGCharacter::StaticClass();
	PlayerControllerClass = AJTGPlayerController::StaticClass();
	SavingSystemClass = USavingSystem::StaticClass();
	CraftingSystemClass = UCraftingSystem::StaticClass();
	GameplaySystemClass = UGameplaySystem::StaticClass();
	InventorySystemClass = UInventorySystem::StaticClass();
}

void AJTGGameMode::InitGameState()
{
	Super::InitGameState();

	if (SavingSystemClass != nullptr)
	{
		SavingSystem = NewObject<USavingSystem>(this, SavingSystemClass);

		auto gameInstance = GetGameInstance<UJTGGameInstance>();

		if (gameInstance)
		{
			auto saveSlot = GetGameInstance<UJTGGameInstance>()->CurrentSaveSlot;

			WasLoaded = SavingSystem->LoadGame(saveSlot);
		}
	}

	if (CraftingSystemClass != nullptr)
	{
		CraftingSystem = NewObject<UCraftingSystem>(this, CraftingSystemClass);
	}

	if (GameplaySystemClass != nullptr)
	{
		GameplaySystem = NewObject<UGameplaySystem>(this, GameplaySystemClass);
	}

	if (InventorySystemClass != nullptr)
	{
		InventorySystem = NewObject<UInventorySystem>(this, InventorySystemClass);

		if (WasLoaded)
		{
			InventorySystem->LoadState(SavingSystem);
		}
	}

	if (ControllerStateSystemClass != nullptr)
	{
		ControllerStateSystem = NewObject<UControllerStateSystem>(this, ControllerStateSystemClass);
	}
}
