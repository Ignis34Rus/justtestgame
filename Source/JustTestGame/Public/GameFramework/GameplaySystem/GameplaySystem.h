// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "GameplaySystemShared.h"
#include "GameplaySystem.generated.h"

class UGameplaySystemStorage;

/** Gameplay system manager. */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UGameplaySystem : public UObject
{
	GENERATED_BODY()

private:
	/** Gameplay system database. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UGameplaySystemStorage* Storage;

public:
	/** Get gameplay system storage. */
	FORCEINLINE
	UGameplaySystemStorage* GetStorage() const { return Storage; };

};
