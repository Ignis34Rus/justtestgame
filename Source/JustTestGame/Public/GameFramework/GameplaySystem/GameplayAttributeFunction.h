// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayAttributeFunction.generated.h"

class UGameplaySystemComponent;

/**  */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UGameplayAttributeFunction : public UObject
{
	GENERATED_BODY()

public:
	/**  */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Tick(float deltaTime, const UGameplaySystemComponent* component);

	virtual void Tick_Implementation(float deltaTime, const UGameplaySystemComponent* component) { unimplemented() }

};
