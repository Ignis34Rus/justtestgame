// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "GameplaySystemShared.h"
#include "Components/ActorComponent.h"
#include "GameplaySystemComponent.generated.h"

class UGameplayAttributeFunction;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FGameplayAttributeValueChanged, const FGameplayTag&, Attribute, float, NewValue, float, OldValue);

/**  */
UCLASS(Blueprintable, ClassGroup = (GameFramework), meta = (BlueprintSpawnableComponent))
class JUSTTESTGAME_API UGameplaySystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	UGameplaySystemComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/**  */
	UPROPERTY(BlueprintAssignable)
	FGameplayAttributeValueChanged OnAttributeMaximumValueChanged;

	/**  */
	UPROPERTY(BlueprintAssignable)
	FGameplayAttributeValueChanged OnAttributeCurrentValueChanged;

private:
	/**  */
	UPROPERTY(EditAnywhere, meta = (Categories = "GameplayAttribute.Stat"))
	TMap<FGameplayTag, FGameplayAttribute> Attributes;

	/**  */
	UPROPERTY(EditAnywhere, meta = (Categories = "GameplayAttribute.Stat"))
	TMap<FGameplayTag, TSubclassOf<UGameplayAttributeFunction>> AttributeFunctionClasses;

	/**  */
	UPROPERTY()
	TMap<FGuid, FGameplayAttributeModificator> AttributeModificators;

	/**  */
	UPROPERTY()
	TArray<UGameplayAttributeFunction*> AttributeFunctions;

public:
	/**  */
	virtual void BeginPlay() override;

	/**  */
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE TMap<FGameplayTag, FGameplayAttribute> GetAttributes() const { return Attributes; }

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE bool HasAttribute(const FGameplayTag& id) const { return Attributes.Contains(id); }

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE float GetAttributeCurrentValue(const FGameplayTag& id) const { return Attributes.FindChecked(id).GetCurrent(); }

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE float GetAttributeMaximumValue(const FGameplayTag& id) const { return Attributes.FindChecked(id).GetMaximum(); }

	/**  */
	UFUNCTION(BlueprintCallable, BlueprintPure)
	FORCEINLINE float GetAttributeOriginalValue(const FGameplayTag& id) const { return Attributes.FindChecked(id).GetOriginal(); }

	/**  */
	UFUNCTION(BlueprintCallable)
	void SetAttributeCurrentValue(const FGameplayTag& id, const float value);

	/**  */
	UFUNCTION(BlueprintCallable)
	void SetAttributeOriginalValue(const FGameplayTag& id, const float value);

	/**  */
	UFUNCTION(BlueprintCallable)
	void IncrementAttributeCurrentValue(const FGameplayTag& id, const float value)
	{
		SetAttributeCurrentValue(id, GetAttributeCurrentValue(id) + FMath::Abs(value));
	}

	/**  */
	UFUNCTION(BlueprintCallable)
	void DecrementAttributeCurrentValue(const FGameplayTag& id, const float value)
	{
		SetAttributeCurrentValue(id, GetAttributeCurrentValue(id) - FMath::Abs(value));
	}

	/**  */
	UFUNCTION(BlueprintCallable)
	FGuid AddAttributeModifier(const FGameplayTag& id, float value);

	/**  */
	UFUNCTION(BlueprintCallable)
	FGuid AddAttributeMultiplier(const FGameplayTag& id, float value);

	/**  */
	UFUNCTION(BlueprintCallable)
	bool RemoveAttributeModificator(const FGuid& guid);

private:
	/**  */
	FORCEINLINE void RiseAttributeValueChanged(const FGameplayTag& id, const float& oldMaximum, const float& oldCurrent) const
	{
		const auto& attribute = Attributes.FindChecked(id);
		const auto newMaximum = attribute.GetMaximum();
		const auto newCurrent = attribute.GetCurrent();
		if (oldMaximum != newMaximum)
		{
			OnAttributeMaximumValueChanged.Broadcast(id, newMaximum, oldMaximum);
		}
		if (oldCurrent != newCurrent)
		{
			OnAttributeCurrentValueChanged.Broadcast(id, newCurrent, oldCurrent);
		}
	}

};
