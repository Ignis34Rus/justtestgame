// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GameplaySystemStorageShared.h"
#include "GameplaySystemStorage.generated.h"

/** Gameplay system database. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UGameplaySystemStorage : public UDataAsset
{
	GENERATED_BODY()

private:
	/** All gameplay attribute. */
	UPROPERTY(EditAnywhere, meta = (Categories = "GameplayAttribute"))
	TMap<FGameplayTag, FGameplayAttributeInfo> Attributes;

public:
	/** Get gameplay attribute info by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "GameplayAttribute"))
	const FGameplayAttributeInfo& GetAttributeInfo(const FGameplayTag& id) const { return Attributes.FindChecked(id); }
};
