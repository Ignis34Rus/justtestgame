// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "GameplaySystemShared.generated.h"

/**  */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FGameplayAttribute
{
	GENERATED_BODY()

public:
	FGameplayAttribute()
	{
		MulValue = 1;
	}

private:
	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Maximum Value"))
	float MaxValue;

	/**  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Current Value"))
	float CurValue;

	/**  */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true", DisplayName = "Original Value"))
	float OrgValue;

	/**  */
	float ModValue;

	/**  */
	float MulValue;

public:
	/**  */
	FORCEINLINE float GetCurrent() const { return CurValue; }

	/**  */
	FORCEINLINE void SetCurrent(float value) { CurValue = FMath::Clamp(value, .0f, MaxValue); }

	/**  */
	FORCEINLINE float GetMaximum() const { return MaxValue; }

	/**  */
	FORCEINLINE float GetOriginal() const { return OrgValue; }

	/**  */
	FORCEINLINE void SetOriginal(float value)
	{
		OrgValue = value;
		Recalculate();
	}

	/**  */
	FORCEINLINE void AddModifier(float value)
	{
		ModValue += value;
		Recalculate();
	}

	/**  */
	FORCEINLINE void RemoveModifier(float value)
	{
		ModValue -= value;
		Recalculate();
	}

	/**  */
	FORCEINLINE void AddMultiplier(float value)
	{
		MulValue += value;
		Recalculate();
	}

	/**  */
	FORCEINLINE void RemoveMultiplier(float value)
	{
		MulValue -= value;
		Recalculate();
	}

	/**  */
	FORCEINLINE void Recalculate()
	{
		auto ratio = CurValue && MaxValue ? CurValue / MaxValue : 1;

		MaxValue = (OrgValue + ModValue) * MulValue;
		CurValue = MaxValue * ratio;
	}
};

/**  */
UENUM(BlueprintType)
enum class EGameplayAttributeModificatorType : uint8
{
	Multiplier,
	Modifier,
};

/**  */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FGameplayAttributeModificator
{
	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(BlueprintReadWrite)
	float Value;

	/**  */
	UPROPERTY(BlueprintReadWrite)
	FGameplayTag AttributeId;

	/**  */
	UPROPERTY(BlueprintReadWrite)
	EGameplayAttributeModificatorType Type;

};
