// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "GameplaySystemStorageShared.generated.h"

class UGameplayAttributeFunction;

/** Item database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FGameplayAttributeInfo
{
	GENERATED_BODY()

public:
	/** Gameplay attribute Name. */
	UPROPERTY(EditAnywhere)
	FText Name;

	/** Gameplay attribute Description. */
	UPROPERTY(EditAnywhere)
	FText Description;

	/** Gameplay attribute Short Name. */
	UPROPERTY(EditAnywhere)
	FText ShortName;

	/** Gameplay attribute Function class. */
	UPROPERTY(EditAnywhere)
	TSubclassOf<UGameplayAttributeFunction> FunctionClass;
};
