// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControllerStateSystemStorageShared.generated.h"

/**  */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FControllerStateInfo
{
	GENERATED_BODY()
	
public:
	/**  */
	UPROPERTY(EditDefaultsOnly)
	FText Name;
	
	/**  */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<class UControllerState> StateClass;

};
