// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "Engine/DataAsset.h"
#include "ControllerStateSystemStorageShared.h"
#include "ControllerStateSystemStorage.generated.h"

/**  */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UControllerStateSystemStorage : public UDataAsset
{
	GENERATED_BODY()

private:
	/**  */
	UPROPERTY(EditDefaultsOnly, meta = (Categories = "ControllerState"))
	TMap<FGameplayTag, FControllerStateInfo> States;

public:
	/**  */
	UFUNCTION(BlueprintCallable)
	const FControllerStateInfo& GetControllerStateInfo(const FGameplayTag& id) const { return States.FindChecked(id); }

};
