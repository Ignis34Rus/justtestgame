// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "ControllerStateSystem.generated.h"

class UControllerState;
class UControllerStateSystemStorage;

/**  */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UControllerStateSystem : public UObject
{
	GENERATED_BODY()

private:
	/**  */
	UPROPERTY(EditDefaultsOnly)
	UControllerStateSystemStorage* Storage;

public:
	/**  */
	UFUNCTION(BlueprintCallable)
	UControllerState* GetState(const FGameplayTag& id);
};
