// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ControllerState.generated.h"

class AJTGPlayerController;

/**  */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UControllerState : public UObject
{
	GENERATED_BODY()
	
public:
	/**  */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void EnterState(const AJTGPlayerController* controller);

	virtual void EnterState_Implementation(const AJTGPlayerController* controller) { unimplemented() }
	
	/**  */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void LeaveState(const AJTGPlayerController* controller);

	virtual void LeaveState_Implementation(const AJTGPlayerController* controller) { unimplemented() }
};
