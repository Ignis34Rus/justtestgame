// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SavingSystemShared.h"
#include "InventorySystemShared.h"
#include "JTGSaveGame.generated.h"

/** Game save. */
UCLASS()
class JUSTTESTGAME_API UJTGSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	/** Saved player state. */
	UPROPERTY()
	FPlayerStateInfo PlayerState;

	/** Saved dynamic items instances. */
	UPROPERTY()
	TArray<FItemInstanceInfo> ItemsInstances;

	/** Saved world objects state. */
	UPROPERTY()
	TMap<FString, FWorldObjectState> WorldObjectStates;

};
