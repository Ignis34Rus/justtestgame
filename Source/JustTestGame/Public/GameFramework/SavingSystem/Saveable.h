// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SavingSystemShared.h"
#include "Saveable.generated.h"

/** Saveable interface. */
UINTERFACE(Blueprintable)
class JUSTTESTGAME_API USaveable : public UInterface
{
	GENERATED_BODY()
};

/** Saveable interface. */
class JUSTTESTGAME_API ISaveable
{
	GENERATED_BODY()
	
public:
	/** Save state of object. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	const FWorldObjectState SaveState();

	/** Load state of object. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void LoadState(UPARAM(ref)const FWorldObjectState& info);
};
