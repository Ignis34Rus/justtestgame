// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ItemBase.h"
#include "InventorySystemShared.h"
#include "SavingSystemShared.generated.h"

/** Item instance save entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemInstanceInfo
{
	GENERATED_BODY()

public:
	/** Item guid */
	UPROPERTY(BlueprintReadOnly)
	FItemGuid Guid;

	/** Item max stack size. */
	UPROPERTY(BlueprintReadOnly)
	int32 StackSize;
	
	/** Item base price. */
	UPROPERTY(BlueprintReadOnly)
	int32 BasePrice;

	/** [NotImplemented] Item sockets. */
	UPROPERTY(BlueprintReadOnly)
	TArray<FItemSocketSlot> Sockets;

	/** Item Attributes. */
	UPROPERTY(BlueprintReadOnly, meta = (Categories = "ItemAttribute"))
	TMap<FGameplayTag, int32> ItemAttributes;

	/** Item Gameplay Attributes. */
	UPROPERTY(BlueprintReadOnly, meta = (Categories = "GameplayAttribute"))
	TMap<FGameplayTag, float> GameplayAttributes;

};

/** World object state entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FWorldObjectState
{
	GENERATED_BODY()

public:
	/**  */
	UPROPERTY(BlueprintReadWrite)
	FVector Position;

	/**  */
	UPROPERTY(BlueprintReadWrite)
	FRotator Rotation;

	/**  */
	UPROPERTY(BlueprintReadWrite)
	TSubclassOf<class AActor> ObjectClass;

	/** World object gameplay attributes. */
	UPROPERTY(BlueprintReadWrite, meta = (Categories = "GameplayAttribute"))
	TMap<FGameplayTag, int32> GameplayAttributes;

};

/** Save slot entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FSaveSlotInfo
{
	GENERATED_BODY()

public:
	/** Save slot Name. */
	UPROPERTY(BlueprintReadWrite)
	FString SlotName;

	/** Save slot Index. */
	UPROPERTY(BlueprintReadWrite)
	int32 SlotIndex;

	/** Save slot Save Date. */
	UPROPERTY(BlueprintReadWrite)
	FDateTime SaveDate;
};

/** Item stack save entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemStackInfo
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	FItemStackInfo(): Guid(), Index(), Amount() {}

	/** Copy from item stack constructor. */
	FItemStackInfo(const FItemStack& other) 
	{
		Guid = other.Item->Guid;
		Index = other.Index;
		Amount = other.Amount;
	}

public:
	/** Item guid. */
	UPROPERTY(BlueprintReadOnly)
	FItemGuid Guid;

	/** Stack index. */
	UPROPERTY(BlueprintReadOnly)
	int32 Index;

	/** Amount of items in this stack. */
	UPROPERTY(BlueprintReadOnly)
	int32 Amount;

};

/** Player state save entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FPlayerStateInfo
{
	GENERATED_BODY()

public:
	/** Player crafting recipes. */
	UPROPERTY()
	TSet<FGameplayTag> Recipes;

	/** Player inventory. */
	UPROPERTY()
	TArray<FItemStackInfo> MainInventory;

	/** Player position. */
	UPROPERTY()
	FVector Position;

	/** Player rotation. */
	UPROPERTY()
	FRotator Rotation;

	/** Control rotation. */
	UPROPERTY()
	FRotator ControlRotation;

};
