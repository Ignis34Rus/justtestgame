// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SavingSystemShared.h"
#include "GameFramework/SaveGame.h"
#include "JTGMainMenuSaveGame.generated.h"

/** Stores info about all save game slots. */
UCLASS(BlueprintType)
class JUSTTESTGAME_API UJTGMainMenuSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	/** Last saved game. */
	UPROPERTY(BlueprintReadWrite)
	FSaveSlotInfo ContinueGame;

	/** [NotImplemented] Save slots. */
	UPROPERTY(BlueprintReadOnly)
	TArray<FSaveSlotInfo> Slots;

};
