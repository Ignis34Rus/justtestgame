// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SavingSystemShared.h"
#include "SavingSystem.generated.h"

class UItemBase;

/*
* Save system manager. 
* ToDo: use some kind of binary serialization
*/
UCLASS(Blueprintable)
class JUSTTESTGAME_API USavingSystem : public UObject
{
	GENERATED_BODY()

private:
	/** Player state. */
	UPROPERTY()
	FPlayerStateInfo PlayerState;

	/** Dynamic items instances. */
	UPROPERTY()
	TArray<FItemInstanceInfo> ItemsInstances;
	
public:
	/** Load game state from slot. */
	UFUNCTION(BlueprintCallable)
	bool LoadGame(const FSaveSlotInfo& slotInfo);

	/** Load game state to slot. */
	UFUNCTION(BlueprintCallable)
	bool SaveGame(const FSaveSlotInfo& slotInfo);

	/** Save items instances. */
	void SaveItemInstance(const FItemInstanceInfo& instanceInfo);

	/** Save player state. */
	FORCEINLINE void SavePlayer(const FPlayerStateInfo& playerInfo) { PlayerState = playerInfo; }

	/** Get player state. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE FPlayerStateInfo& GetPlayerState() { return PlayerState; }

	/** Get items instances. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE TArray<FItemInstanceInfo>& GetItemsInstances() { return ItemsInstances; }

};
