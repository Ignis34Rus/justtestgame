// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "InteractionSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractableCandidateRemoved, const TScriptInterface<IInteractable>, Candidate);

class IInteractable;
class UPrimitiveComponent;

/** Component that detects interactable object in specific range. */
UCLASS(Blueprintable, ClassGroup = (GameFramework), meta = (BlueprintSpawnableComponent))
class JUSTTESTGAME_API UInteractionSystemComponent : public USphereComponent
{
	GENERATED_BODY()

public:	
	/** Default constructor. */
	UInteractionSystemComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/** Minimum value of dot product between camera and interactable object. Used for filtering in 'Best Interactable' selection. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MinInteractionDot;

	/** Called when candidat removed from internal collection. */
	UPROPERTY(BlueprintAssignable)
	FInteractableCandidateRemoved OnInteractableCandidateRemoved;

private:
	/** Collection of available interaction candidates. */
	UPROPERTY()
	TArray<TScriptInterface<IInteractable>> Candidates;

public:
	/** Get best interaction candidate based on dot product between camera and interactable object. */
	UFUNCTION(BlueprintCallable)
	TScriptInterface<IInteractable> GetBestInteractable(const class UCameraComponent* camera) const;

private:
	/** Called when object enters interaction range. */
	UFUNCTION()
	void InteractionOnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Called when object leaves interaction range. */
	UFUNCTION()
	void InteractionOnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
