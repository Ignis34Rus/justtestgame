// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.generated.h"

/** Interactable interface. */
UINTERFACE(Blueprintable)
class JUSTTESTGAME_API UInteractable : public UInterface
{
	GENERATED_BODY()
};

/** Interactable interface. */
class JUSTTESTGAME_API IInteractable
{
	GENERATED_BODY()
	
public:
	/** Position of interactable object. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	FVector GetPosition() const;

	/** [NotImplemented] Priority of interactable object. */
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	int32 GetPriority() const;

	/** Interact with object. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Interact(const class AJTGPlayerController* controller);

	/** Set highlighting of object. Called when it selected/deselected by player. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void SetHighlight(const class AJTGPlayerController* controller, bool value);

};
