// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "GameFramework/Actor.h"
#include "InteractableBase.generated.h"

/** Actor with Interactable interface. */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API AInteractableBase : public AActor, public IInteractable
{
	GENERATED_BODY()

};
