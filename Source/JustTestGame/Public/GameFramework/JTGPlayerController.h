// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interactable.h"
#include "InventorySystemShared.h"
#include "GameFramework/PlayerController.h"
#include "JTGPlayerController.generated.h"

class UUserWidget;
class AJTGCharacter;
class UInventoryWidget;
class UControllerState;

/** Custom player controller. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API AJTGPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	AJTGPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

private:
	/** Custom character. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	AJTGCharacter* JTGCharacter;

	/** Default class of inventory widget. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UInventoryWidget> InventoryWidgetClass;

	/** Inventory widget instance. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventoryWidget* InventoryWidget;

	/** Default class of ingame menu widget. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> InGameMenuWidgetClass;
		
	/** Ingame menu widget instance. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UUserWidget* InGameMenuWidget;

	/**  */
	UPROPERTY(EditDefaultsOnly, meta = (Categories = "ControllerState"))
	FGameplayTag InventoryStateTag;

	/**  */
	UPROPERTY(EditDefaultsOnly, meta = (Categories = "ControllerState"))
	FGameplayTag IngameMenuStateTag;

	/**  */
	UPROPERTY()
	TArray<UControllerState*> States;

	/** Flag: Is it first call to set pawn. */
	bool FirstTimeLoad = true;

public:
	/** Default SetPawn override */
	virtual void SetPawn(APawn* InPawn) override;

	/** Save current game. */
	UFUNCTION(BlueprintCallable)
	void SaveGame();

	/**  */
	UFUNCTION(BlueprintCallable)
	void PopState();

	/**  */
	UFUNCTION(BlueprintCallable)
	void PushState(const FGameplayTag& id);

	/** Called when best interaction candidate is changed. */
	UFUNCTION(BlueprintNativeEvent)
	void OnBestInteractableCandidateChanged(const TScriptInterface<IInteractable>& oldCandidate, const TScriptInterface<IInteractable>& newCandidate);

	/** Native implementation of OnBestInteractableCandidateChange. */
	virtual void OnBestInteractableCandidateChanged_Implementation(const TScriptInterface<IInteractable>& oldCandidate, const TScriptInterface<IInteractable>& newCandidate);

	/** Try pause game. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool TrySetGamePaused(bool isPaused) { return SetPause(isPaused); }

	/** Get custom character. */
	FORCEINLINE AJTGCharacter* GetJTGCharacter() const { return JTGCharacter; }

protected:
	/** Default SetupInputComponent override */
	virtual void SetupInputComponent() override;

	/** Default BeginPlayingState override */
	virtual void BeginPlayingState() override;

private:
	/** Input axis move forward/backwards. */
	void InputMoveForward(float value);

	/** Input axis move right/left. */
	void InputMoveRight(float value);

	/**  */
	void InputTurn(float value);

	/**  */
	void InputLookUp(float value);

	/** Input action jump pressed. */
	void InputJumpPressed();

	/** Input action jump released. */
	void InputJumpReleased();

	/** Input action inventory toggle. */
	void InputInventoryTogglePressed();

	/** Input action inventory toggle. */
	void InputInteractPressed();

	/** Input action save game. */
	void InputSaveGamePressed();

	/** Input action ingame menu. */
	void InputInGameMenuPressed();

	/** Called when item dropped from inventory slot. */
	UFUNCTION()
	void OnSlotDropped(const FItemStack& slot);

};
