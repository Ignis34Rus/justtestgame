// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "JTGGameMode.generated.h"

class USavingSystem;
class UCraftingSystem;
class UGameplaySystem;
class UInventorySystem;
class UControllerStateSystem;

/** Custom game mode. */
UCLASS()
class JUSTTESTGAME_API AJTGGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	AJTGGameMode();

private:
	/** Is game loaded frome save or it`s new game. */
	UPROPERTY()
	bool WasLoaded;

	/** Default class of saving system. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<USavingSystem> SavingSystemClass;

	/** Saving system instance. */
	UPROPERTY()
	USavingSystem* SavingSystem;

	/** Default class of crafting system. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UCraftingSystem> CraftingSystemClass;

	/** Crafting system instance. */
	UPROPERTY()
	UCraftingSystem* CraftingSystem;

	/** Default class of gameplay system. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGameplaySystem> GameplaySystemClass;

	/** Gameplay system instance. */
	UPROPERTY()
	UGameplaySystem* GameplaySystem;

	/** Default class of inventory system. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UInventorySystem> InventorySystemClass;

	/** Inventory system instance. */
	UPROPERTY()
	UInventorySystem* InventorySystem;

	/** Default class of controller state system. */
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UControllerStateSystem> ControllerStateSystemClass;

	/** Controller state system instance. */
	UPROPERTY()
	UControllerStateSystem* ControllerStateSystem;

public:
	/** GameModeBase InitGameState override */
	virtual void InitGameState() override;

	/** Check is game loaded frome save or it`s new game. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE bool GameWasLoaded() const { return WasLoaded; };

	/** Get saving system instance. */
	UFUNCTION(BlueprintCallable) 
	FORCEINLINE USavingSystem* GetSavingSystem() const { return SavingSystem; };

	/** Get crafting system instance. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE UCraftingSystem* GetCraftingSystem() const { return CraftingSystem; };

	/** Get gameplay system instance. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE UGameplaySystem* GetGameplaySystem() const { return GameplaySystem; };

	/** Get inventory system instance. */
	UFUNCTION(BlueprintCallable) FORCEINLINE
	UInventorySystem* GetInventorySystem() const { return InventorySystem; };

	/** Get controller state system instance. */
	UFUNCTION(BlueprintCallable) 
	FORCEINLINE UControllerStateSystem* GetControllerStateSystem() const { return ControllerStateSystem; };

};
