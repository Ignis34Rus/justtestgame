// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "JTGCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBestInteractableCandidateChanged, const TScriptInterface<IInteractable>&, OldCandidate, const TScriptInterface<IInteractable>&, NewCandidate);

class UCameraComponent;
class USpringArmComponent;
class UCraftingSystemComponent;
class UInventorySystemComponent;
class UInteractionSystemComponent;
class UGameplaySystemComponent;

/** Custom character. */
UCLASS()
class JUSTTESTGAME_API AJTGCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	AJTGCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/** Event fired when best interaction candidate is changed. */
	UPROPERTY(BlueprintAssignable)
	FBestInteractableCandidateChanged OnBestInteractableCandidateChanged;

private:
	/** Main camera. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;

	/** Main camera anchor. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraAnchor;

	/** Gameplay system component. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UGameplaySystemComponent* GameplaySystem;

	/** Crafting system component. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UCraftingSystemComponent* CraftingSystem;

	/** Inventory system component. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UInventorySystemComponent* InventorySystem;

	/** Interaction system component. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Gameplay, meta = (AllowPrivateAccess = "true"))
	UInteractionSystemComponent* InteractionSystem;

	/** Best interaction candidate. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	TScriptInterface<IInteractable> BestInteractionCandidate;

public:
	/** Actor Tick method override. */
	virtual void Tick(float DeltaTime) override;

	/** Get main camera. */
	FORCEINLINE 
	UCameraComponent* GetCamera() const { return Camera; }

	/** Get gameplay system component. */
	FORCEINLINE 
	UGameplaySystemComponent* GetGameplaySystem() const { return GameplaySystem; }

	/** Get crafting system component. */
	FORCEINLINE 
	UCraftingSystemComponent* GetCraftingSystem() const { return CraftingSystem; }

	/** Get inventory system component. */
	FORCEINLINE 
	UInventorySystemComponent* GetInventorySystem() const { return InventorySystem; }

	/** Get interaction system component. */
	FORCEINLINE 
	UInteractionSystemComponent* GetInteractionSystem() const { return InteractionSystem; }

	/** Get best interaction candidate. */
	FORCEINLINE 
	TScriptInterface<IInteractable> GetBestInteractionCandidate() const { return BestInteractionCandidate; }

private:
	/** Subscriber of OnInteractableCandidateRemoved event in InteractionSystem. */
	UFUNCTION()
	void OnInteractableCandidateRemoved(const TScriptInterface<class IInteractable> candidate);
};
