// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "SavingSystemShared.h"
#include "JTGGameInstance.generated.h"

/** Custom game instance. */
UCLASS(BlueprintType)
class UJTGGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	UJTGGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/** Current save slot. */
	UPROPERTY(BlueprintReadWrite)
	FSaveSlotInfo CurrentSaveSlot;

};
