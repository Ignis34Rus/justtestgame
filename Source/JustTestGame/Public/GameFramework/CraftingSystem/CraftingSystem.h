// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CraftingSystemShared.h"
#include "CraftingSystem.generated.h"

class UCraftingSystemStorage;
class UInventorySystemComponent;

/** Crafting system manager. */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UCraftingSystem : public UObject
{
	GENERATED_BODY()

private:
	/** Crafting recipes database. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UCraftingSystemStorage* Storage;

public:
	/** Checks craft requirements and if they satisfied remove reagents/add crafted item. */
	UFUNCTION(BlueprintCallable)
	bool Craft(const FGameplayTag& recipe, UInventorySystemComponent* inventory);

	/** Get crafting info by recipe id, fill requirements. */
	UFUNCTION(BlueprintCallable)
	FCraftingInfo GetCraftingInfo(const FGameplayTag& recipe, const UInventorySystemComponent* inventory) const;
	
	/** Get crafting system storage. */
	FORCEINLINE UCraftingSystemStorage* GetStorage() const { return Storage; };

};
