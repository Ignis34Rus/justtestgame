// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "CraftingSystemStorageShared.h"
#include "CraftingSystemStorage.generated.h"

/** Recipes database. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UCraftingSystemStorage : public UDataAsset
{
	GENERATED_BODY()

private:
	/** All game recipes. */
	UPROPERTY(EditAnywhere, meta = (Categories = "CraftingIdentity"))
	TMap<FGameplayTag, FCraftingRecipeInfo> Recipes;

public:
	/** Get recipe by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "CraftingIdentity"))
	const FCraftingRecipeInfo& GetCraftingRecipeInfo(const FGameplayTag& id) const { return Recipes.FindChecked(id); }

};
