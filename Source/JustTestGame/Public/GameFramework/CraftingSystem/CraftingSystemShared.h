// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "CraftingSystemShared.generated.h"

class UItemBase;

/** Requirement of craft recipe, with available amount. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FCraftingRequirement
{
	GENERATED_BODY()

public:
	/** Requirement item. */
	UPROPERTY(BlueprintReadOnly)
	UItemBase* Item;

	/** Available amount of item. */
	UPROPERTY(BlueprintReadOnly)
	int32 AmountAvailable;

	/** Required amount of item. */
	UPROPERTY(BlueprintReadOnly)
	int32 AmountRequired;
};

/** Crafting info, with requred for ui data. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FCraftingInfo
{
	GENERATED_BODY()

public:
	/** Recipe id. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTag Id;

	/** Name of craft recipe. */
	UPROPERTY(BlueprintReadOnly)
	FText Name;

	/** Description of craft recipe. */
	UPROPERTY(BlueprintReadOnly)
	FText Description;

	/** Id of item that recipe produce. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTag Produce;

	/** Name of item that recipe produce. */
	UPROPERTY(BlueprintReadOnly)
	FText ProduceName;

	/** Minimum produce items count. */
	UPROPERTY(BlueprintReadOnly)
	int32 ProduceMin;
	
	/** Maximum produce items count. */
	UPROPERTY(BlueprintReadOnly)
	int32 ProduceMax;

	/** Requirements of craft recipe. */
	UPROPERTY(BlueprintReadOnly)
	TArray<FCraftingRequirement> Requirements;
};
