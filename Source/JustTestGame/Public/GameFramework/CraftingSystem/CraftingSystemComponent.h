// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "Components/ActorComponent.h"
#include "CraftingSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCraftingRecipesChanged);

/** Actor component for storing available recipes. */
UCLASS(Blueprintable, ClassGroup = (GameFramework), meta = (BlueprintSpawnableComponent))
class JUSTTESTGAME_API UCraftingSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	/** Notify when internal collection of recipes changed. */
	UPROPERTY(BlueprintAssignable)
	FCraftingRecipesChanged OnCraftingRecipesChanged;

private:
	/** Available recipes. */
	UPROPERTY()
	TSet<FGameplayTag> Recipes;

public:
	/** Add new recipe. */
	UFUNCTION(BlueprintCallable)
	void Add(const FGameplayTag& recipe);

	/** Remove recipe. */
	UFUNCTION(BlueprintCallable)
	void Remove(const FGameplayTag& recipe);

	/** Get all available recipes. */
	UFUNCTION(BlueprintCallable)
	FORCEINLINE TSet<FGameplayTag>& GetRecipes() { return Recipes; };
	
	/** Load recipes without rising changed event. */
	FORCEINLINE void LoadState(const TSet<FGameplayTag>& state) { Recipes = state; };

};
