// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "CraftingSystemStorageShared.generated.h"

/** Recipe requirement database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FCraftingRequirementInfo
{
	GENERATED_BODY()

public:
	/** Id of requirement item. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemIdentity"))
	FGameplayTag ItemId;

	/** Amount of requirement item. */
	UPROPERTY(EditAnywhere)
	int32 Amount;

};

/** Recipe database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FCraftingRecipeInfo
{
	GENERATED_BODY()

public:
	/** Recipe name. */
	UPROPERTY(EditAnywhere)
	FText Name;

	/** Recipe description. */
	UPROPERTY(EditAnywhere)
	FText Description;

	/** Id of item that recipe produce. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemIdentity"))
	FGameplayTag Produce;
	
	/** Minimum produce items count. */
	UPROPERTY(EditAnywhere)
	int32 ProduceMin;
	
	/** Maximum produce items count. */
	UPROPERTY(EditAnywhere)
	int32 ProduceMax;

	/** Requirements of craft recipe. */
	UPROPERTY(EditAnywhere)
	TArray<FCraftingRequirementInfo> Requirements;

};
