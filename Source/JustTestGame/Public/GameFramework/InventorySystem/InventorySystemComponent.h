// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "Components/ActorComponent.h"
#include "InventorySystemShared.h"
#include "InventorySystemComponent.generated.h"

class UItemBase;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInventoryDropped, const FItemStack&, Slot);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInventorySlotChanged, const FItemStack&, Slot);

/** Actor component that store items. */
UCLASS(Blueprintable, ClassGroup = (GameFramework), meta = (BlueprintSpawnableComponent))
class JUSTTESTGAME_API UInventorySystemComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:
	/** Max slots count, 0 for infinity.  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxSlots;

	/** Categories of items that can be stored in this inventory. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Categories = "ItemCategory"))
	FGameplayTagContainer Categories;

	/** Event that rises when item dropped from inventory slot. */
	UPROPERTY(BlueprintAssignable)
	FInventoryDropped OnSlotDropped;

	/** Event that rises when inventory slot changed. */
	UPROPERTY(BlueprintAssignable)
	FInventorySlotChanged OnSlotChanged;

private:
	/** Cache of items count. */
	UPROPERTY()
	TMap<FItemGuid, int32> CountCache;

	/** Inventory slots array. */
	UPROPERTY()
	TArray<FItemStack> Slots;

public:
	/** Get count of specific item. */
	UFUNCTION(BlueprintPure)
	int Count(const UItemBase* item) const;

	/** Checks if specific count of item can be added to inventory. */
	UFUNCTION(BlueprintCallable)
	bool CanBeAdded(const UItemBase* item, const int32 count);

	/** Add count of item to inventory. */
	UFUNCTION(BlueprintCallable)
	int Add(const UItemBase* item, const int32 count);

	/** Remove count of item from inventory. */
	UFUNCTION(BlueprintCallable)
	int Remove(const UItemBase* item, const int32 count);
	
	/** Remove items from specific slot. */
	UFUNCTION(BlueprintCallable)
	bool RemoveSlot(const int32 slotIndex, FItemStack& stack);

	/** Remove items from slot to slot. */
	UFUNCTION(BlueprintCallable)
	bool Move(const int32 sourceSlotIndex, const int32 destinationSlotIndex);

	/** Compacts inventory slots without merging stacks. */
	UFUNCTION(BlueprintCallable)
	void Compact();

	/** Get all inventory slots. */
	UFUNCTION(BlueprintCallable)
	TArray<FItemStack> GetSlots();

	/** Request drop items in slot. */
	UFUNCTION(BlueprintCallable)
	bool RequestDrop(int slotIndex);

	/** Load state. */
	UFUNCTION()
	void LoadState(const TArray<FItemStack>& state);

	/** Checks if inventory has slots limit. */
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool HasMaxSlots() const { return MaxSlots > 0; }

private:
	/** Get first free slot index. */
	UFUNCTION()
	int GetFreeIndex();

	/** Get slot at specified index. */
	UFUNCTION()
	FItemStack& GetSlot(const int32 slotIndex, int32& arrayIndex);

};
