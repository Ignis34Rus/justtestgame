// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "InventorySystemShared.generated.h"

/** Item guid. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemGuid
{
	GENERATED_BODY()

public:
	/** Item Id. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (Categories = "ItemIdentity"))
	FGameplayTag Id;

	/** Instance unique id. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Uid;

public:
	/** Checks if item is dynamic or static. */
	FORCEINLINE bool IsInstance() const { return Uid > 0; }

	FORCEINLINE bool operator ==(const FItemGuid& other) const
	{
		return Id == other.Id && Uid == other.Uid;
	}
};

/** Used so we can have a TMap of this struct */
 FORCEINLINE uint32 GetTypeHash(const FItemGuid& value)
 {
	 return HashCombine(GetTypeHash(value.Id), GetTypeHash(value.Uid));
 }

/** Item stack. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemStack
{
	GENERATED_BODY()

public:
	/** Item. */
	UPROPERTY(BlueprintReadOnly)
	const class UItemBase* Item;

	/** Stack index. */
	UPROPERTY(BlueprintReadOnly)
	int32 Index;

	/** Amount of items in this stack. */
	UPROPERTY(BlueprintReadOnly)
	int32 Amount;

};

/** [NotImplemented] Item socket slot. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemSocketSlot
{
	GENERATED_BODY()

public:
	/** Soket type id. */
	UPROPERTY(BlueprintReadOnly, meta = (Categories = "ItemSoket"))
	FGameplayTag Socket;

	/** Item id. */
	UPROPERTY(BlueprintReadWrite, meta = (Categories = "ItemIdentity"))
	FGameplayTag Item;

};
