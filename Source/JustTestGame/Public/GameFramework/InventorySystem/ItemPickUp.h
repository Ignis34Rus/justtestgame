// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Saveable.h"
#include "Interactable.h"
#include "InventorySystemShared.h"
#include "GameFramework/Actor.h"
#include "ItemPickUp.generated.h"

/** Base actor for item pickups. */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API AItemPickUp : public AActor, public IInteractable, public ISaveable
{
	GENERATED_BODY()

public:
	/** Default constructor. */
	AItemPickUp(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:
	/** Item Guid. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FItemGuid ItemGuid;

	/** Item Amount. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Amount;

private:
	/** Interaction sphere, allows this actor to be detected by interaction system. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* InteractionSphere;
	
	/** Widget that visible when actor is highlighted. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UWidgetComponent* InteractionWidget;

	/** Constructed item. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	const class UItemBase* Item;
	
	/** Controller that highlighted this actor. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	const class AJTGPlayerController* Controller;

public:
	/** Default Tick override. */
	virtual void Tick(float DeltaTime) override;

	/** Position of interactable object. */
	virtual FVector GetPosition_Implementation() const override;
	
	/** Interact with object. */
	virtual void Interact_Implementation(const class AJTGPlayerController* controller);

	/** Set highlighting of object. Called when it selected/deselected by player. */
	virtual void SetHighlight_Implementation(const class AJTGPlayerController* controller, bool value);

protected:
	/** Default BeginPlay override. */
	virtual void BeginPlay() override;
};
