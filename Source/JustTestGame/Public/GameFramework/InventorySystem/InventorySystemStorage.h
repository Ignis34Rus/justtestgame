// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "InventorySystemStorageShared.h"
#include "InventorySystemStorage.generated.h"

/** Inventory system database. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UInventorySystemStorage : public UDataAsset
{
	GENERATED_BODY()

private:
	/** All game items. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemIdentity"))
	TMap<FGameplayTag, FItemInfo> Items;
	
	/** Item quality configurations. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemQuality"))
	TMap<FGameplayTag, FItemQualityInfo> Qualities;
	
	/** Item category configurations. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemCategory"))
	TMap<FGameplayTag, FItemCategoryInfo> Categories;

	/** Item material configurations. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemMaterial"))
	TMap<FGameplayTag, FItemMaterialInfo> Materials;

public:
	/** Get item info by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "ItemIdentity"))
	const FItemInfo& GetItemInfo(const FGameplayTag& id) const { return Items.FindChecked(id); }

	/** Get quality info by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "ItemQuality"))
	const FItemQualityInfo& GetQualityInfo(const FGameplayTag& id) const { return Qualities.FindChecked(id); }

	/** Get category info by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "ItemCategory"))
	const FItemCategoryInfo& GetCategoryInfo(const FGameplayTag& id) const { return Categories.FindChecked(id); }

	/** Get material info by id. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "ItemMaterial"))
	const FItemMaterialInfo& GetMaterialInfo(const FGameplayTag& id) const { return Materials.FindChecked(id); }
};
