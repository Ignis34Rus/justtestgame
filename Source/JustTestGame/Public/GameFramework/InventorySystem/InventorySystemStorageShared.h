// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "InventorySystemStorageShared.generated.h"

/** Item database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemInfo
{
	GENERATED_BODY()

public:
	/** Item Name. */
	UPROPERTY(EditAnywhere)
	FText Name;

	/** Item Description. */
	UPROPERTY(EditAnywhere)
	FText Description;

	/** Item Icon. */
	UPROPERTY(EditAnywhere)
	class UPaperSprite* Icon;

	/** [NotImplemented] Item Script. */
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UItemScript> Script;

	/** Class of actor used for item pickup/drop. */
	UPROPERTY(EditAnywhere)
	TSubclassOf<class AItemPickUp> PickUpVisual;

	/** [NotImplemented] Item use spell. */
	UPROPERTY(EditAnywhere, meta = (Categories = "SpellIdentity"))
	FGameplayTag Spell;

	/** Item primary category. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemCategory"))
	FGameplayTag Category;

	/** [NotImplemented] Item sockets. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemSoket"))
	TArray<FGameplayTag> Sockets;

	/** Item secondary categories. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemCategory"))
	FGameplayTagContainer SubCategories;

	/** Item Material. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemMaterial"))
	FGameplayTag Material;

	/** Item Quality. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemQuality"))
	FGameplayTag Quality;

	/** Item Attributes. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemAttribute"))
	TMap<FGameplayTag, int> ItemAttributes;

	/** Item Gameplay Attributes. */
	UPROPERTY(EditAnywhere, meta = (Categories = "GameplayAttribute"))
	TMap<FGameplayTag, float> GameplayAttributes;

};

/** Item Quality database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemQualityInfo
{
	GENERATED_BODY()

public:
	/** Quality Name. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FText Name;

	/** Quality Color. */
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FColor Color;

};


/** Item Category database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemCategoryInfo
{
	GENERATED_BODY()

public:
	/** Category Name. */
	UPROPERTY(EditAnywhere)
	FText Name;

	/** Category based item attributes. */
	UPROPERTY(EditAnywhere, meta = (Categories = "ItemAttribute"))
	TMap<FGameplayTag, int> ItemAttributes;

};

/** Item Materia database entity. */
USTRUCT(BlueprintType)
struct JUSTTESTGAME_API FItemMaterialInfo
{
	GENERATED_BODY()

public:
	/** Materia Name. */
	UPROPERTY(EditAnywhere)
	FText Name;

};
