// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemScript.generated.h"

/** [NotImplemented] Item Script. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UItemScript : public UObject
{
	GENERATED_BODY()
	
};
