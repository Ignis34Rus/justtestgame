// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "InventorySystemShared.h"
#include "ItemBase.generated.h"

class UInventorySystem;

/** Constructed item. */
UCLASS(BlueprintType)
class JUSTTESTGAME_API UItemBase : public UObject
{
	GENERATED_BODY()

	friend UInventorySystem;

public:
	/** Item Guid. */
	UPROPERTY(BlueprintReadOnly)
	FItemGuid Guid;

	/** Item Name. */
	UPROPERTY(BlueprintReadOnly)
	FText Name;

	/** Item Description. */
	UPROPERTY(BlueprintReadOnly)
	FText Description;

	/** Item max stack size. */
	UPROPERTY(BlueprintReadOnly)
	int32 StackSize;
	
	/** Item base price. */
	UPROPERTY(BlueprintReadOnly)
	int32 BasePrice;

	/** Item Icon. */
	UPROPERTY(BlueprintReadOnly)
	class UPaperSprite* Icon;

	/** Class of actor used for item pickup/drop. */
	UPROPERTY(BlueprintReadOnly)
	TSubclassOf<class AItemPickUp> PickUpVisual;

	/** [NotImplemented] Item sockets. */
	UPROPERTY(BlueprintReadOnly)
	TArray<FItemSocketSlot> Sockets;

	/** Item categories. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTagContainer Categories;

	/** [NotImplemented] Item use spell. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTag Spell;

	/** Item Material. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTag Material;

	/** Item Quality. */
	UPROPERTY(BlueprintReadOnly)
	FGameplayTag Quality;

	/** Item Attributes. */
	UPROPERTY(BlueprintReadOnly)
	TMap<FGameplayTag, int> ItemAttributes;

	/** Item Gameplay Attributes. */
	UPROPERTY(BlueprintReadOnly)
	TMap<FGameplayTag, float> GameplayAttributes;

private:

	/** Inventory system that constructed this item. */
	UPROPERTY(BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventorySystem* InventorySystem;

public:
	/**  */
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool Equals(const UItemBase* other) const { return Guid == other->Guid; }

	/** Checks if item is dynamic or static. */
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsInstance() const { return Guid.IsInstance(); }

	/** Checks if item can be stacked. */
	UFUNCTION(BlueprintPure)
	FORCEINLINE bool IsStackable() const { return StackSize > 1; }

	/** Get inventory system that constructed this item. */
	FORCEINLINE UInventorySystem* GetInventorySystem() const { return InventorySystem; }
};
