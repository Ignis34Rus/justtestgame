// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Slate/SObjectWidget.h"
#include "InventoryWidget.generated.h"

class UInventorySystemComponent;

/** Base class for inventory widgets. */
UCLASS(Blueprintable)
class JUSTTESTGAME_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	/** Inventory columns count. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Columns;

	/** Inventory system that bonded to this widget. */
	UPROPERTY(BlueprintReadWrite)
	UInventorySystemComponent* InventorySystem;

};
