// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTags.h"
#include "InventorySystemShared.h"
#include "InventorySystem.generated.h"

class UItemBase;
class USavingSystem;
class UInventorySystemStorage;

/** Inventory system manager. */
UCLASS(Abstract, Blueprintable)
class JUSTTESTGAME_API UInventorySystem : public UObject
{
	GENERATED_BODY()

private:
	/** Item attribute for getting max stack size property. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (Categories = "ItemAttribute", AllowPrivateAccess = "true"))
	FGameplayTag StackSizeTag;

	/** Item attribute for getting base price property. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (Categories = "ItemAttribute", AllowPrivateAccess = "true"))
	FGameplayTag BasePriceTag;

	/** Inventory system database. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UInventorySystemStorage* Storage;

	/** [NotImplemented] Cache of dynamic items. */
	UPROPERTY()
	TMap<FItemGuid, UItemBase*> DynamicItemsCache;

	/** Cache of static items. */
	UPROPERTY()
	TMap<FGameplayTag, UItemBase*> StaticItemsCache;

	/** [NotImplemented] Counters for dynamic items uid generation. */
	UPROPERTY()
	TMap<FGameplayTag, int32> DynamicItemsUidCounters;

public:
	/** Create new item. Static items use same instance stored in cache. */
	UFUNCTION(BlueprintCallable, meta = (Categories = "ItemIdentity"))
	UItemBase* CreateItem(const FGameplayTag& id);
	
	/** Get item by guid. */
	UFUNCTION(BlueprintCallable)
	UItemBase* GetItem(const FItemGuid& guid);

	/** Load state of Inventory System. */
	UFUNCTION(BlueprintCallable)
	void LoadState(USavingSystem* savingSystem);

	/** Save state of Inventory System. */
	UFUNCTION(BlueprintCallable)
	void SaveState(USavingSystem* savingSystem);

	/** Get inventory system storage. */
	FORCEINLINE UInventorySystemStorage* GetStorage() const { return Storage; };

private:
	/** Construct item. */
	UItemBase* ConstructItem(const FItemGuid& guid);
};
